#!/usr/bin/env python
import json
import telepot
import time
import requests
from tools import *



def handle(msg):
    chat_id = msg['chat']['id']
    command = msg['text']
    #print(time.strftime('[%d %b, %y %r] ' + str(chat_id) + ': ' + command))

    command = command.replace(' --force', '')
    bot.sendChatAction(chat_id, 'typing')

    if command.startswith('/start'):
        bot.sendMessage(chat_id, 'Starting Kodi')

    elif command.startswith('/test'):
        data = '{"id":529,"jsonrpc":"2.0","method":"Player.Seek", "params": {"playerid":1, "value":{"hours": 1}} }'

    elif command.startswith('/host'):
        command = command.split(' ')[1:]
        bot.sendMessage(chat_id, '{0}'.format(HOST))

    elif command.startswith('/url'):
        command = command.split(' ')[1:]
        data = '{"id":529,"jsonrpc":"2.0","method":"Player.Open","params":{"item":{"file":"' + ''.join(command) + '"}}}'
        print(post(data).text)
        bot.sendMessage(chat_id, 'Playing url')

    elif command.startswith('/pause'):
        data = '{"id":529,"jsonrpc":"2.0","method":"Player.PlayPause","params":{"playerid": 1}}}'
        req = post(data)
        result = json.loads(req.text)
        try:
            if result['result']['speed'] == 1:
                bot.sendMessage(chat_id, 'Playing')
            else:
                bot.sendMessage(chat_id, 'Pause')
        except:
            bot.sendMessage(chat_id, 'Error')
    else:
        data = '[{"jsonrpc":"2.0","method":"Input.SendText","params":["' + command + '"],"id":10}]'
        post(data)
        bot.sendMessage(chat_id, 'Text sent')


bot = telepot.Bot('520061711:AAHPd5Ce1qZGujeSSzK2pNYm5hU3A1FBGW0')
bot.message_loop(handle)
print('I am listening ...')

while True:
    time.sleep(10)
