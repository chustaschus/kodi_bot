import re
import time
from bs4 import BeautifulSoup
from selenium import webdriver


class Openload:
    def __init__(self, time_wait=10, headless=False, download=False):
        self.headless = headless
        self.time_wait = time_wait
        self.download = download
        if self.headless:
            options = webdriver.FirefoxOptions()
            options.add_argument('--headless')
            self.driver = webdriver.Firefox(firefox_options=options)
        else:
            self.driver = webdriver.Firefox()

    def get_video(self, url):
        self.driver.get(url)
        principal = self.driver.current_window_handle
        time.sleep(self.time_wait)
        try:
            ads = self.driver.find_element_by_id('videooverlay')
            ads.click()
        except:
            pass
        button = self.driver.find_element_by_class_name('vjs-big-play-button')

        soup = BeautifulSoup(self.driver.page_source, 'html5lib')
        self.driver.switch_to.window(principal)
        video = soup.find('video', {'id': 'olvideo_html5_api'}).get('src')
        if video:
            if self.download:
                url = 'https://oload.stream/stream/{0}'.format(video)
            else:
                url = 'https://oload.stream/stream/{0}?mime=true'.format(video)
            return url

    def finish(self):
        self.driver.quit()

