import re
import time
from bs4 import BeautifulSoup
from selenium import webdriver


class Streamango:
    def __init__(self, time_wait=5, headless=False):
        self.headless = headless
        self.time_wait = time_wait
        if self.headless:
            options = webdriver.FirefoxOptions()
            options.add_argument('--headless')
            self.driver = webdriver.Firefox(firefox_options=options)
        else:
            self.driver = webdriver.Firefox()

    def get_video(self, url):
        self.driver.get(url)
        time.sleep(self.time_wait)
        soup = BeautifulSoup(self.driver.page_source, 'html5lib')
        url = soup.find('video', {'id': 'mgvideo_html5_api'})
        if url is not None:
            return 'https:{0}'.format(url.get('src'))
        else:
            return None

    def finish(self):
        self.driver.quit()
