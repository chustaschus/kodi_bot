import re
import time
from bs4 import BeautifulSoup
from selenium import webdriver


class Streamcloud:
    def __init__(self, time_wait=15, headless=False):
        self.headless = headless
        self.time_wait = time_wait

        if self.headless:
            options = webdriver.FirefoxOptions()
            options.add_argument('--headless')
            self.driver = webdriver.Firefox(firefox_options=options)
        else:
            self.driver = webdriver.Firefox()

    def get_video(self, url):
        self.driver.get(url)
        time.sleep(self.time_wait)
        button = self.driver.find_element_by_id('btn_download')
        button.click()
        soup = BeautifulSoup(self.driver.page_source, 'html5lib')
        regex = r"(http://[\w_-]+[\w.,@?^=%&:/~+#-]*[\w@?^=%&/~+#-]video.mp4)"
        video = ''
        for a in soup.find_all('script'):
            matches = re.search(regex, str(a))
            if matches is not None:
                video = matches.group()
        return video

    def finish(self):
        self.driver.quit()

st = Streamcloud(headless=True)
a = st.get_video('http://streamcloud.eu/vjusfb94wik1/la.leyenda.de.ben.hall.hdr.bravier.avi.html')
st.finish()
print(a)