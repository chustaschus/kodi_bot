import requests
from pymongo import MongoClient

HOST = "192.168.1.4"
PORT = 8080


def post(data):
    req = requests.post('http://{0}:{1}/jsonrpc'.format(HOST, PORT), data=data)
    return req